from functions import m_json
from functions import m_pck

m_pck.check_sensors()
path = "calorimetry_home/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path)

folder_path = "calorimetry_home/datasheets"
metadata_updated = m_json.add_temperature_sensor_serials(folder_path, metadata)
print(metadata_updated)

data = m_pck.get_meas_data_calorimetry(metadata_updated)


m_pck.logging_calorimetry(data,metadata_updated,"calorimetry_home/setup_newton",folder_path)

m_json.archiv_json(folder_path, path, "calorimetry_home/data")



